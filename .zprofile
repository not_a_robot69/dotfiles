# include ~/.local/bin in $PATH
export PATH="$PATH:$HOME/.local/bin"

# default programs
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="chromium"

# cleanup home directory
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XINITRC="${XDG_CONFIG_HOME:-$HOME/.config}/x11/xinitrc"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export ZDOTDIR="$HOME/.config/zsh"

# autostart Xorg if in tty1
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  startx
fi
