local cmd = vim.cmd
cmd 'packadd paq-nvim'         	   -- Load package
local paq = require'paq-nvim'.paq  -- Import module and bind `paq` function
paq{'savq/paq-nvim', opt=true}     -- Let Paq manage itself

-- packages

-- orgmode plugin for vim/nvim :)
-- paq 'jceb/vim-orgmode'
-- preview markdown in a browser tab
-- paq 'davidgranstrom/nvim-markdown-preview'
-- cooler status line
paq 'hoob3rt/lualine.nvim'
-- icons for nvim
paq 'kyazdani42/nvim-web-devicons'
-- show startup time
paq 'tweekmonster/startuptime.vim'
-- train yourself with vim/nvim motions
paq 'tjdevries/train.nvim'
-- sets a different colour for each pair of parentheses
paq 'frazrepo/vim-rainbow'
-- plugin that shows git diff in left column
paq 'airblade/vim-gitgutter'
-- git integration with vim/nvim
paq 'tpope/vim-fugitive'
-- plugin to make multiple cursors
paq 'mg979/vim-visual-multi'
-- comment functions plugin
paq 'preservim/nerdcommenter'
-- filesystem explorer for vim/nvim
-- paq 'ms-jpq/chadtree'
-- TOML syntax for vim/nvim
-- paq 'cespare/vim-toml'
-- helps in managing crates versions in Cargo.toml files
-- paq 'mhinz/vim-crates'
-- DOOM Emacs theme for vim/nvim
-- paq 'romgrk/doom-one.vim'
-- let regions in current buffer to use different syntax from current buffer's filetype
paq 'inkarkat/vim-SyntaxRange'
-- lsp
paq 'neovim/nvim-lspconfig'
-- autocompletion framework using nvim's built in LSP
paq 'nvim-lua/completion-nvim'
-- use treesitter to improve highlighting
paq {'nvim-treesitter/nvim-treesitter', run='TSUpdate'}
-- make scrolling smoother
paq 'psliwka/vim-smoothie'
-- material theme for nvim
paq 'marko-cerovac/material.nvim'
-- run snippets of code independent of the rest of the file
paq {'michaelb/sniprun', run='bash install.sh'}
-- popup window of possible keybindings
paq 'folke/which-key.nvim'
-- write files with root priviliges
paq 'lambdalisue/suda.vim'

-- end of packages

local scopes = {o = vim.o, b = vim.bo, w = vim.wo}

local function opt(scope, key, value)
  scopes[scope][key] = value
  if scope ~= 'o' then scopes['o'][key] = value end
end

local indent = 2

-----------------OPTIONS-----------------
opt('b', 'expandtab', true)           -- use spaces instead of tabs
opt('b', 'shiftwidth', indent)        -- size of an indent
opt('b', 'tabstop', indent)           -- number of spaces tabs count for
opt('b', 'smartindent', true)         -- insert indents automatically
opt('o', 'ignorecase', true)          -- ignore case
opt('o', 'splitbelow', true)          -- put new windows below current
opt('o', 'splitright', true)          -- pit new windows right of current
opt('o', 'termguicolors', true)       -- true colour support
opt('w', 'number', true)              -- line number
opt('o', 'updatetime', 200)           -- set update time to 0.2 seconds
cmd 'let g:completion_trigger_keyword_length = 3'
cmd 'let mapleader = " "'             -- set leader key to space
cmd 'colorscheme material'            -- enable material theme
vim.g.material_style = "deep ocean"   -- set material theme to palenight
vim.g.timeoutlen = 100
-- make background transparent
-- cmd 'highlight Normal guibg=none'
-- cmd 'highlight NonText guibg=none'
-- make background transparent

-----------------MAPPINGS-----------------

local function map(mode, lhs, rhs, opts)
  local options = {noremap = true}
  if opts then options = vim.tbl_extend('force', options, opts) end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end
-- map('', '<leader>c', '"+y')           -- copy to clipboard in all modes
map('n', '<C-l>', '<cmd>noh<CR>')     -- clear highlights
-- use <Tab> and <S-Tab> to navigate through popup menu with completion-nvim
cmd 'inoremap <expr> <Tab>   pumvisible() ? "<C-n>" : "<Tab>"'
cmd 'inoremap <expr> <S-Tab> pumvisible() ? "<C-p>" : "<S-Tab>"'
-- autospell toggle with <F6>
cmd 'set nospell spelllang=en_gb'
cmd 'nnoremap <silent> <F6> :set invspell<cr>'
cmd 'inoremap <silent> <F6> <C-O>:set invspell<cr>'

----------------- OTHER -----------------
cmd 'let g:nvim_markdown_preview_theme = "solarized-dark"'

-- require'lspconfig'.rls.setup{on_attach=require'completion'.on_attach}         -- configure rust lsp with lspconfig for autocompletion
require'lspconfig'.rls.setup{}
require'lspconfig'.ccls.setup{}
require('lualine').setup{
  options = {
    theme = 'material-nvim'            -- set lualine theme to material
  }
}
require'sniprun'.setup({
  display = { "TempFloatingWindow" },  -- make sniprun use a temporary floating window
})
require("which-key").setup{}           -- enable which-key

-- set all files with the .ms extension to nroff files.
cmd ':autocmd BufRead,BufNewFile *.ms set filetype=nroff'
-- automatically compile groff_ms documents into a pdf of the same name when writing the file
cmd ':autocmd BufWritePost *.ms !groff -ms -Tpdf % > %:r.pdf'
